use std::fs;
use std::io::Read;
use std::ops::{Add, Sub};
use std::path::Path;
use std::time::SystemTime;

use chrono::{DateTime, Duration};
use serde_xml_rs::{from_reader,Error};
use zip;

use crate::date_parser::{Date, Dura};
use crate::gpx_analyser::render;
use crate::mongo_api::store;

//const EARTH_RADIUS_M: f64 = 6_371_008.8;
const ELE_CHANGE_THRESHOLD: f64 = 0.63;
const MOVING_SPEED_THRESHOLD_KPH: f64 = 0.25;

type Result<T> = std::result::Result<T, failure::Error>;

#[derive(Deserialize, Serialize, Debug, Default)]
pub struct Gpx {
    #[serde(default)]
    pub _id: String,
    #[serde(default)]
    pub metadata: Option<MetaData>,
    #[serde(rename = "trk", default)]
    pub track: Option<Track>,
    #[serde(default)]
    pub analysis: Option<GpxAnalysis>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct MetaData {
    #[serde(rename = "name", )]
    pub name: String,
    #[serde(rename = "desc", )]
    pub description: String,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Track {
    #[serde(rename = "name")]
    pub name: String,
    #[serde(rename = "desc")]
    pub description: String,
    #[serde(rename = "type", alias = "type")]
    pub track_type: String,
    #[serde(rename = "trkseg", default)]
    pub track_segments: Vec<TrackSegment>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct TrackSegment {
    #[serde(rename = "trkpt")]
    pub track_points: Vec<TrackPoint>
}

#[serde(rename = "trkpt")]
#[derive(Deserialize, Serialize, Debug)]
pub struct TrackPoint {
    #[serde(rename = "lat")]
    pub latitude: f64,
    #[serde(rename = "lon")]
    pub longitude: f64,
    #[serde(rename = "ele", default)]
    pub elevation: f64,
    pub time: Date,
}

///Holds the Track analysis-data
#[derive(Default, Debug, Serialize, Deserialize)]
pub struct GpxAnalysis {
    key: String,
    start_time: Date,
    stop_time: Date,
    pub total_time: Dura,
    total_moving_time: Dura,
    average_speed_kph: f64,
    average_moving_speed_kph: f64,
    average_pace_mpk: f64,
    average_moving_pace_mpk: f64,

    pub distance: f64,
    distance_flat: f64,
    distance_ascending: f64,
    distance_descending: f64,
    total_elevate_up: f64,
    total_elevate_down: f64,
    highest_elevation: f64,
    lowest_elevation: f64,
    elevation_difference: f64,
}


impl Gpx {
    pub fn get_id(&self) -> String {
        self._id.to_owned()
    }
}


impl GpxAnalysis {
    ///Creates a GpxAnalystis ready for use.
    pub fn with_key(key: String) -> GpxAnalysis {
        let mut g = GpxAnalysis::default();
        g.key = key;
        g.highest_elevation = f64::MIN;
        g.lowest_elevation = f64::MAX;
        g.start_time = DateTime::parse_from_str("9968-04-14 00:00:00 +0000", "%F %T %z").unwrap().into();
        g.stop_time = DateTime::parse_from_str("0066-02-23 00:00:00 +0000", "%F %T %z").unwrap().into();
        g
    }

    ///Updates the distance-related data with the Track-step-data provided.
    pub fn update_distances(&mut self, current_elevation: f64, elevation_change: f64, distance_change: f64) {

        //Determine the elevation extremes...
        if current_elevation < self.lowest_elevation {
            self.lowest_elevation = current_elevation;
        }
        if current_elevation > self.highest_elevation {
            self.highest_elevation = current_elevation;
        }

        //Only include an elevation change when it is higher or lower than the threshhold...
        if elevation_change >= ELE_CHANGE_THRESHOLD {
            self.total_elevate_up += elevation_change;
            self.distance_ascending += distance_change;
        } else if elevation_change <= -ELE_CHANGE_THRESHOLD {
            self.total_elevate_down += elevation_change;
            self.distance_descending += distance_change;
        } else {
            self.distance_flat += distance_change;
        }

        //Add to the total distance...
        self.distance += distance_change;

        //determine the elevation-difference so far...
        self.elevation_difference = self.highest_elevation - self.lowest_elevation;
    }

    ///Updates the time-related-analysis data with the Track-step-data provided.
    pub fn update_times(&mut self, tp_time: &Date, duration: Duration, distance_change: f64) {

        //Update start and stop times...
        if tp_time.get().lt(&self.start_time.get()) {
            self.start_time = tp_time.clone();
        }
        if tp_time.get().gt(&self.stop_time.get()) {
            self.stop_time = tp_time.clone();
        }

        //Total time
        self.total_time = self.stop_time.get().sub(self.start_time.get()).into();

        //Moving time...
        if Self::speed_kph(distance_change, duration) >= MOVING_SPEED_THRESHOLD_KPH {
            self.total_moving_time = self.total_moving_time.get().add(duration).into()
        }

        //Average Speeds...
        self.average_speed_kph = Self::speed_kph(self.distance, self.total_time.get());
        self.average_moving_speed_kph = Self::speed_kph(self.distance, self.total_moving_time.get());

        //Average Speeds...
        self.average_pace_mpk = 60.0 / (Self::speed_kph(self.distance, self.total_time.get()));
        self.average_moving_pace_mpk = 60.0 / (Self::speed_kph(self.distance, self.total_moving_time.get()));
    }

    fn speed_kph(m: f64, t: Duration) -> f64 {
        3.6 * 1000_000.0 * m / t.num_microseconds().unwrap() as f64
    }
}

pub fn onboard(gpx_data: &Path) -> Result<String> {
    println!("Processing file {:?}", gpx_data);

    let mut result = Ok(String::from(format!("Could not read {:?}. Is at a zip-file, gpx-file or a directory?", gpx_data)));
    if gpx_data.is_dir() {
        result = read_dir(gpx_data);
    } else if gpx_data.is_file() && gpx_data.extension().unwrap().eq("zip") {
        result = process_zip(gpx_data);
    } else if gpx_data.is_file() && gpx_data.extension().unwrap().eq("gpx") {
        result = store_gpx_file(&mut fs::File::open(gpx_data).unwrap());
    }

    if let Err(e) = result {
        result = Ok(format!("An error occurred whilst processing Gpx-file {:?}: {:?}", gpx_data, e));
    }
    result
}

fn process_zip(gpx_zip: &Path) -> Result<String> {
    let start = SystemTime::now();
    let mut zip = zip::ZipArchive::new(fs::File::open(gpx_zip)?)?;

    for i in 0..zip.len() {
        let mut file = zip.by_index(i)?;
        println!("Filename: {}", file.name());
        if file.is_file() { store_gpx_file(&mut file)?; }
    }
    Ok(format!("Processed Zip-file {:?} containing {} files in {:?}", gpx_zip, zip.len(), start.elapsed().unwrap()))
}

fn read_dir(gpx_dir: &Path) -> Result<String> {
    Ok(format!("Processing GPX-Directory {:?}", gpx_dir))
}


fn store_gpx_file(xml_reader: &mut dyn Read) -> Result<String> {
    let start = SystemTime::now();
    let mut track = create_gpx(xml_reader)?;
    track.analysis = Some(render(&track)?);
    store(&vec!(&track))?;
    Ok(format!("Stored GPX-File {:#?} in {:?}: {:#?}", track.get_id(), start.elapsed().unwrap(), track.analysis))
}

fn create_gpx(xml_reader: &mut dyn Read) -> Result<Gpx> {
    let mut track: Gpx = from_reader(xml_reader)?;
    if let Some(id) = track.metadata.as_ref() {
        track._id = id.name.clone();
        Ok(track)
    }else{
        Err(Error::Custom{ field: "Could not create GPX".to_string()}.into())
    }
}

