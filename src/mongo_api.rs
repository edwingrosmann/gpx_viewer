use std::time::SystemTime;

use bson::from_bson;
use mongodb::bson::{Bson, doc, Document};
use mongodb::Client;
use mongodb::options::FindOptions;
use tokio::stream::StreamExt;

use crate::gpx_data::Gpx;

const HOST: &'static str = "localhost";
const DATABASE: &'static str = "test";
const COLLECTION_NAME: &'static str = "gpx_data";
const ID_FIELD: &'static str = "_id";

type Result<T> = std::result::Result<T, failure::Error>;


///For field options see MongoDb 'projections' something like ```doc!{"analysis": true}```
pub fn select_like(id_fragment: &str, fields_option:  impl Into<Option<Document>>) -> Result<Vec<Gpx>> {
    read_some(doc! {"_id":{"$regex":id_fragment }}, options(fields_option.into()), &create_mongo_client(HOST)?, DATABASE, COLLECTION_NAME)
}

pub fn _select_all() -> Result<Vec<Gpx>> {
    read_some(None, None, &create_mongo_client(HOST)?, DATABASE, COLLECTION_NAME)
}

pub fn select_by_keys(keys: Vec<&str>,fields_option:Option<Document>) -> Option<Vec<Gpx>> {
    let client = create_mongo_client(HOST).expect("Could not create aMongoDB Client");
    let start = SystemTime::now();

    match read_some(doc! {"_id":{"$in":keys}}, options(fields_option), &client, DATABASE, COLLECTION_NAME) {
        Ok(data) => {
            println!("Loading Gpx-data from cache in {:?}", start.elapsed().unwrap());
            if data.len() > 0 { Some(data) } else { None }
        }
        Err(e) => {
            println!("Could not load Gpx-data: {}", e.to_string());
            None
        }
    }
}

pub fn store(new_data: &Vec<&Gpx>) -> Result<String> {
    //Create a MongoDB-client...
    let client = create_mongo_client(HOST).expect("Could not create a MongoDB Client");

    let start = SystemTime::now();

    //Store all the gpx...
    for page_data in new_data {
        upsert(
            &client,
            DATABASE,
            "gpx_data",
            &mut bson::to_document(&page_data)
                .expect("Could not create Document from Gpx-Data")
                .clone(),
        )?
    }
    Ok(format!("Stored Gpx-data {:?} in: {:?}", new_data.iter().map(|e| e.get_id()).collect::<String>(), start.elapsed().unwrap()))
}

#[tokio::main]
async fn create_mongo_client(host: &str) -> mongodb::error::Result<Client> {
    Ok(Client::with_uri_str(format!("mongodb://{}:27017", host).as_str()).await?)
}

#[tokio::main]
async fn upsert(
    client: &Client,
    database: &str,
    collection_name: &str,
    data: &mut Document,
) -> Result<()> {
    let collection = client.database(database).collection(collection_name);

    //Find by key-field...
    let q = &doc! {"_id":data.get_str("_id").unwrap()};

    //Upsert: If the document exists then update otherwise insert.
    //Empirical Observation: Updating and inserting takes about the same...
    if collection.count_documents(q.clone(), None).await? > 0 {
        collection.update_one(q.clone(), data.clone(), None).await?;
        println!(
            "Updated Gpx-data with key {:?} into collection: {}/{}",
            get_id(data),
            database,
            collection_name
        );
    } else {
        println!(
            "Inserted New Gpx-data with key {} into collection: {}/{}",
            collection.insert_one(data.clone(), None).await?.inserted_id,
            database,
            collection_name
        );
    }
    Ok(())
}

#[tokio::main]
async fn read_some(
    find_filter: impl Into<Option<Document>>,
    options: impl Into<Option<FindOptions>>,
    client: &Client,
    database: &str,
    collection_name: &str,
) -> Result<Vec<Gpx>> {
    let collection = client.database(database).collection(collection_name);

    let fil = &find_filter.into().unwrap();
    println!("Find Filter: {:#?}", fil);

    //Find by keys...
    let all_gpx_data = &mut collection
        .find(fil.clone(), options)
        .await?;

    //Select all those valid documents that could be converted to Gpx-structs...
    let  result: Vec<Gpx> = //Vec::new();
        all_gpx_data
            .filter(std::result::Result::is_ok)
            .map(|g| from_bson::<Gpx>(Bson::from(&g.unwrap())))
            .filter(std::result::Result::is_ok)
            .map(|g| g.unwrap())
            .collect().await;

    println!(
        "MongoDb Found Documents with Keys: {:?}:",
        result.iter().map(Gpx::get_id).collect::<Vec<String>>()
    );
    Ok(result)
}

fn options(fields_option: Option<Document>) -> FindOptions {
    FindOptions::builder()
        .sort(doc! {"_id": 1})
        .projection(fields_option)
        .show_record_id(Some(true))
        .build()
}

fn get_id(data: &Document) -> &str {
    data.get_str(ID_FIELD).unwrap()
}
