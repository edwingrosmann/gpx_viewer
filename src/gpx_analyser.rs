use geoutils;

use mongodb::bson::doc;
use crate::gpx_data::{Gpx, TrackPoint, TrackSegment, GpxAnalysis};
use crate::mongo_api::{select_by_keys, select_like};
use std::ops::Sub;

type Result<T> = std::result::Result<T, failure::Error>;

pub fn print_gpx_data(mongo_key: &str) {

    println!("All Hennys:{:#?}", select_like("hen",doc! {"analysis": true,}).unwrap());

    if let Some(gpx_data) = select_by_keys(vec![mongo_key],None) {
        gpx_data.iter().for_each(|g| println!("GPX-analysis: {:#?}", render(g).unwrap()));
    }
}


pub fn render(gpx: &Gpx) -> Result<GpxAnalysis> {
    let mut analysis = GpxAnalysis::with_key(gpx._id.to_owned());

    //Analyse the distance of the track...
    analyse_track(gpx, &mut analysis);

    Ok(analysis)
}


fn analyse_track(gpx: &Gpx, analysis: &mut GpxAnalysis) {
    if let Some(segs) =  &gpx.track { segs.track_segments.iter().for_each(|t| tally_track_points(&t, analysis))};
}

fn tally_track_points(t: &TrackSegment, analysis: &mut GpxAnalysis) {
    let count = t.track_points.len() - 1;

    // In order to calculate speeds, distances and elevation-changes, a track-point and its
    // adjacent track-point are to be considered...
    for i in 0..count {
        if i < count {
            update_analysis(&t.track_points[i], &t.track_points[i + 1], analysis);
        }
    }
}

///Provide the current track-point, the next and the updatable analysis-struct
fn update_analysis(tp: &TrackPoint, tp_next: &TrackPoint, analysis: &mut GpxAnalysis) {

    //Elevation Change...
    let d_ele = tp_next.elevation - tp.elevation;

    //The distance traveled, ignoring elevation...
    let mut d_dist = geoutils::Location::new(tp_next.latitude, tp_next.longitude)
        .distance_to(&geoutils::Location::new(tp.latitude, tp.longitude)).unwrap().meters();

    //Also consider the elevation change when computing the change in distance; that's only fair :-)...
    d_dist = (d_ele.powi(2) + d_dist.powi(2)).sqrt();

    //The duration of the next step...
    let d_time = tp_next.time.get().sub(tp.time.get());

    //Feed the analysis with this data..
    analysis.update_distances(tp.elevation, d_ele, d_dist);
    analysis.update_times(&tp.time, d_time, d_dist);
}
