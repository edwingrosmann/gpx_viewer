extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_xml_rs;

use std::path::Path;

use gpx_analyser::print_gpx_data;
use gpx_data as processor;
use settings::args;

mod settings;
mod gpx_analyser;
mod gpx_data;
mod date_parser;
mod mongo_api;

#[cfg(test)]
mod tests;

fn main() {
    init_settings();
    onboard();

    show_gpx_data()
}


fn init_settings() {

    //Get the application-arguments and Remove the first (the programme-name)...
    let mut args = std::env::args().collect::<Vec<String>>();
    args.remove(0);

    //Populate the settings...
    settings::set_args(args);
}

fn onboard() {
    if !settings::do_onboard() {
        return;
    }


    //Advise how to use this tool is filenames have not been provided...
    if args().len() == 0 {
        println!("Please provide a source folder or a zip-file containing gpx-files. e.g. 'cargo run -- \"./gpx_data/gpx_2020_09_20.zip\"");
        return;
    }

    //Process the filenames...
    for gpx_file_names in args() {
        println!("{}", processor::onboard(Path::new(&gpx_file_names)).unwrap());
    }
}

fn show_gpx_data() {
    if settings::do_show() {
        print_gpx_data(&settings::gpx_key());
    }
}
