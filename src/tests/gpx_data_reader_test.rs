use std::path::Path;

use crate::gpx_data as reader;

#[test]
fn read_zip_test() {
    assert!(&reader::onboard(Path::new("./gpx_data/gpx_2020_09_20.zip")).unwrap().contains(r#"Processed Zip-file "./gpx_data/gpx_2020_09_20.zip" containing 285 files"#));
}

#[test]
fn read_directory_test() {
    assert_eq!(&reader::onboard(Path::new("./gpx_data")).unwrap(), r#"Processing GPX-Directory "./gpx_data""#);
}

#[test]
fn read_gpx_file_test() {
    assert!(&reader::onboard(Path::new("./gpx_data/St Hellier Loop_w_henny_2020-08-09 07_36.gpx")).unwrap().contains(r#"tored GPX-File "St Hellier Loop/w/henny/2020-08-09 07:36" in "#));
}

#[test]
fn read_non_existing_file_test() {
    assert_eq!(&reader::onboard(Path::new("./bibbidi")).unwrap(), r#"Could not read "./bibbidi". Is at a zip-file, gpx-file or a directory?"#);
}

#[test]//FIXME
fn read_non_gpx_extension_test() {
    assert_eq!(&reader::onboard(Path::new("./gpx_data/gpx.xsd")).unwrap(), r#"Could not read "./gpx_data/gpx.xsd". Is at a zip-file, gpx-file or a directory?"#);
}

#[test]
fn read_non_gpx_file_test() {
    assert!(&reader::onboard(Path::new("./gpx_data/not_a.gpx")).is_ok());
}