use std::cell::RefCell;
use std::str::FromStr;

// A global variable, limited to the current Thread;
// In order to enable testing on the functions that probe the (immutable) env::args() collection, this contraption has been created:
// It is initialized with the env::args() collection and then the tests can add their test attributes to them...
thread_local!(
    static ARGS: RefCell<Vec<String>> = RefCell::new(Vec::new());
    static TEST_MODE: RefCell<bool> = RefCell::new(true);
);
pub const _DEFAULT_TOPIC: &str = "gpx-data";

pub fn set_args(new_args: Vec<String>) {
    ARGS.with(|args_cell| args_cell.replace(new_args));
    println!("Runtime Arguments: {:?}", args());
}

pub fn _set_test_mode(test: bool) {
    TEST_MODE.with(|mode| mode.replace(test));
    println!("Test-mode: {}", _test_mode());
}

pub fn args() -> Vec<String> {
    ARGS.with(|r| r.borrow().to_vec())
}

pub fn _test_mode() -> bool {
    TEST_MODE.with(|m| m.to_owned().into_inner())
}

pub fn do_onboard() -> bool {
    has_feature_flag("onboard")
}

pub fn do_show() -> bool {
    has_feature_flag("show")
}

pub fn gpx_key() -> String {
    attribute_value("key", String::default())
}

pub fn _kafka_topic() -> String {
    attribute_value("topic", _DEFAULT_TOPIC.to_string())
}

fn has_feature_flag(flag: &str) -> bool {
    args().iter().any(|s| s.to_lowercase().contains(flag))
}

fn attribute_value<T: FromStr>(attribute_name: &str, default_value: T) -> T {
    match args()
        .iter()
        .find(|s| s.to_lowercase().contains(&format!("{}=", attribute_name)))
    {
        Some(key_value_string) => {
            let key_value: Vec<&str> = key_value_string.split('=').collect();
            if key_value.len() == 2 && !key_value[1].is_empty() {
                T::from_str(key_value[1]).unwrap_or(default_value)
            } else {
                default_value
            }
        }
        None => default_value,
    }
}
