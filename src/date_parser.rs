use std::fmt;
use std::fmt::Debug;
use std::result::Result;
use std::str::FromStr;

use bson::Bson;
use chrono::{DateTime, Duration, FixedOffset, Local};
use failure::_core::fmt::Formatter;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use serde::de::{Error, Unexpected, Visitor};

#[derive(Clone)]
pub struct Dura(Duration);

impl From<Duration> for Dura {
    fn from(d: Duration) -> Self {
        Dura(d)
    }
}

impl From<&str> for Dura {
    fn from(d: &str) -> Self {
        let hms: Vec<&str> = d.split(':').collect();

        if hms.len() != 4 {return Dura::default() }
        Dura(Duration::seconds(
            i64::from_str(hms[0]).unwrap_or(0) * 24 * 3600_i64 + //days
                i64::from_str(hms[1]).unwrap_or(0) * 3600_i64 +  //hours
                i64::from_str(hms[2]).unwrap_or(0) * 60_i64 + //minutes
                i64::from_str(hms[3]).unwrap_or(0)) //seconds
        )
    }
}

impl Dura {
    pub fn get(&self) -> Duration {
        self.0
    }
}

impl Default for Dura {
    fn default() -> Self {
        Dura(Duration::seconds(0))
    }
}

impl Debug for Dura {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.write_str(&format!("{} days {} hours {} minutes and {} seconds.", &self.0.num_days(), &self.0.num_hours() % 24, &self.0.num_minutes() % 60, &self.0.num_seconds() % 60))
    }
}

impl Serialize for Dura {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    {
        Bson::String(format!("{:02}:{:02}:{:02}:{:02}",
                             self.0.num_days(),
                             self.0.num_hours() % 24,
                             self.0.num_minutes() % 60,
                             self.0.num_seconds() % 60)).serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for Dura {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
        where
            D: Deserializer<'de>,
    {
        struct DurationVisitor;

        impl<'de> Visitor<'de> for DurationVisitor {
            type Value = Dura;

            fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
                formatter.write_str("Need a Duration String")
            }

            fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
                where
                    E: Error,
            {
                Ok(s.into())
            }
        }
        deserializer.deserialize_str(DurationVisitor)
    }

    fn deserialize_in_place<D>(
        _deserializer: D,
        _place: &mut Self,
    ) -> std::result::Result<(), <D as Deserializer<'de>>::Error>
        where
            D: Deserializer<'de>,
    {
        unimplemented!()
    }
}


#[derive(Clone)]
pub struct Date(DateTime<Local>);

impl Default for Date {
    fn default() -> Self {
        Date(Local::now())
    }
}

impl Date {
    pub fn get(&self) -> DateTime<Local> {
        self.0
    }
}

impl From<DateTime<FixedOffset>> for Date {
    fn from(dt: DateTime<FixedOffset>) -> Self {
        Date(dt.into())
    }
}

impl Debug for Date {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.write_str(&self.0.to_rfc3339())
    }
}

impl Serialize for Date {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
        where
            S: Serializer,
    {
        Bson::String(self.0.to_rfc3339()).serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for Date {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
        where
            D: Deserializer<'de>,
    {
        struct DateVisitor;

        impl<'de> Visitor<'de> for DateVisitor {
            type Value = Date;

            fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
                formatter.write_str("Need a Date String")
            }

            fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
                where
                    E: Error,
            {
                if s.len() > 0 {
                    Ok(Date(
                        DateTime::parse_from_str(&s.replace('Z', "+00:00"), "%+").expect(format!("Not a ISO-Date String: {}", &s).as_str()).into(),
                    ))
                } else {
                    Err(Error::invalid_value(Unexpected::Str(&s), &self))
                }
            }
        }
        deserializer.deserialize_str(DateVisitor)
    }

    fn deserialize_in_place<D>(
        _deserializer: D,
        _place: &mut Self,
    ) -> std::result::Result<(), <D as Deserializer<'de>>::Error>
        where
            D: Deserializer<'de>,
    {
        unimplemented!()
    }
}




